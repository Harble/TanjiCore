﻿var connection = new WebSocketManager.Connection("wss://localhost:8081/packet");
var textArea = document.getElementById("packetLog");
var showIncoming = document.getElementById("showIncoming");
var showOutgoing = document.getElementById("showOutgoing");

connection.enableLogging = false;

connection.connectionMethods.onConnected = () => {
    console.log("You are now connected! Connection ID: " + connection.connectionId);
}

connection.clientMethods["receiveMessage"] = (direction, message) => {
    if (direction == "outgoing" && showOutgoing.checked)
        textArea.value = message + "\r\n" + textArea.value;
    if (direction == "incoming" && showIncoming.checked)
        textArea.value = message + "\r\n" + textArea.value;
};

connection.start();