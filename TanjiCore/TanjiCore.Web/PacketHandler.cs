﻿using System;
using System.Net.WebSockets;
using System.Threading.Tasks;
using TanjiCore.Intercept.Network;
using WebSocketManager;
using WebSocketManager.Common;

namespace TanjiCore.Web
{
    public class PacketHandler : WebSocketHandler
    {
        public PacketHandler(WebSocketConnectionManager webSocketConnectionManager) : base(webSocketConnectionManager)
        {
        }

        public override async Task OnConnected(WebSocket socket)
        {
            await base.OnConnected(socket);

            /*var socketId = WebSocketConnectionManager.GetId(socket);

            var message = new Message()
            {
                MessageType = MessageType.Text,
                Data = $"{socketId} is now connected"
            };

            await SendMessage("outgoing", "test");
            await SendMessage("outgoing", "test");
            await SendMessage("outgoing", "test");
            await SendMessageToAllAsync(message);*/
        }

        public async Task SendMessage(string direction, string message)
        {
            await InvokeClientMethodToAllAsync("receiveMessage", direction, message);
        }

        public override async Task OnDisconnected(WebSocket socket)
        {
            var socketId = WebSocketConnectionManager.GetId(socket);

            await base.OnDisconnected(socket);

            var message = new Message()
            {
                MessageType = MessageType.Text,
                Data = $"{socketId} disconnected"
            };
            await SendMessageToAllAsync(message);
        }

        public void PacketOutgoing(object sender, DataInterceptedEventArgs e)
        {
            var message = Program.Game.OutMessages[e.Packet.Header];

            var structure = "\r\n";
            int position = 0;

            if (message.Structure != null)
            {
                foreach(string valueType in message.Structure)
                {
                    switch (valueType.ToLower())
                    {
                        case "int":
                            structure += ("{i:" + e.Packet.ReadInteger(ref position) + "}");
                            break;
                        case "string":
                            structure += ("{s:" + e.Packet.ReadString(ref position) + "}");
                            break;
                        case "double":
                            structure += ("{d:" + e.Packet.ReadDouble(ref position) + "}");
                            break;
                        case "byte":
                            structure += ("{b:" + e.Packet.ReadByte(ref position) + "}");
                            break;
                        case "boolean":
                            structure += ("{b:" + e.Packet.ReadBoolean(ref position) + "}");
                            break;
                    }
                }
            }

            if (structure != "\r\n") structure += "\r\n";
            SendMessage("outgoing", $"[{message.Hash}]" + structure + $"Incoming: [{e.Packet.Header}]: {e.Packet.ToString()}").Wait();
        }

        public void PacketIncoming(object sender, DataInterceptedEventArgs e)
        {
            var message = Program.Game.InMessages[e.Packet.Header];

            var structure = "\r\n";
            int position = 0;

            if (message.Structure != null)
            {
                foreach (string valueType in message.Structure)
                {
                    switch (valueType.ToLower())
                    {
                        case "int":
                            structure += ("{i:" + e.Packet.ReadInteger(ref position) + "}");
                            break;
                        case "string":
                            structure += ("{s:" + e.Packet.ReadString(ref position) + "}");
                            break;
                        case "double":
                            structure += ("{d:" + e.Packet.ReadDouble(ref position) + "}");
                            break;
                        case "byte":
                            structure += ("{b:" + e.Packet.ReadByte(ref position) + "}");
                            break;
                        case "boolean":
                            structure += ("{b:" + e.Packet.ReadBoolean(ref position) + "}");
                            break;
                    }
                }
            }

            if (structure != "\r\n") structure += "\r\n";
            SendMessage("incoming", $"[{message.Hash}]" + structure + $"Incoming: [{e.Packet.Header}]: {e.Packet.ToString()}").Wait();
        }
    }
}